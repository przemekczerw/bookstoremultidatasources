import com.jayway.restassured.RestAssured
import com.jayway.restassured.response.Response
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.test.context.ContextConfiguration
import spock.lang.Specification

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration
abstract class BookStoreSpec extends Specification {

    private static final Logger log = LoggerFactory.getLogger(BookStoreSpec)

    private static final String CONTENT_TYPE_JSON = 'application/json'

    @LocalServerPort
    protected int port = 8080

    def Response get(String path) {
        RestAssured.given()
                .port(port)
                .contentType(CONTENT_TYPE_JSON)
                .when()
                .get(path)
                .thenReturn()
    }

    def Response post(String path, Object object) {
        RestAssured.given()
                .port(port)
                .contentType(CONTENT_TYPE_JSON)
                .body(object)
                .when()
                .put(path)
                .thenReturn()
    }

    def Response delete(String path) {
        RestAssured.given()
                .port(port)
                .when()
                .delete(path)
                .thenReturn()
    }
}
