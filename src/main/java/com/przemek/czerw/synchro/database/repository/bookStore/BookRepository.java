package com.przemek.czerw.synchro.database.repository.bookStore;

import com.przemek.czerw.synchro.database.entity.bookStore.Book;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BookRepository extends CrudRepository<Book, Long> {

}
