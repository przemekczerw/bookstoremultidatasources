package com.przemek.czerw.synchro.database.controller;

import com.przemek.czerw.synchro.database.entity.bookStore.Book;
import com.przemek.czerw.synchro.database.entity.bookStoreCopy.BookCopy;
import com.przemek.czerw.synchro.database.repository.bookStore.BookRepository;
import com.przemek.czerw.synchro.database.repository.bookStoreCopy.BookCopyRepository;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.SimpleApplicationEventMulticaster;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@RequestMapping("/bookStore")
public class BookController {

    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private BookCopyRepository bookCopyRepository;

    private final ApplicationEventPublisher applicationEventPublisher;

    @Autowired
    public BookController(ApplicationEventPublisher applicationEventPublisher) {
        this.applicationEventPublisher = applicationEventPublisher;
    }


    @GetMapping(value = "/getBook1", produces = "application/json")
    public List<Book> getBook1() {
        return (List<Book>) bookRepository.findAll();
    }

    SimpleApplicationEventMulticaster eventMulticaster = new SimpleApplicationEventMulticaster();

    @GetMapping(value = "/getBook2", produces = "application/json")
    public List<BookCopy> getBook2() {
        return (List<BookCopy>) bookCopyRepository.findAll();
    }


    @PostMapping(path = "/postBook1", consumes = "application/json", produces = "application/json")
    public Book addBook1(@RequestBody Book book) {
        log.info("Book has been added to BookStore1: {}", book);
        applicationEventPublisher.publishEvent(book);
        eventMulticaster.setTaskExecutor(new SimpleAsyncTaskExecutor());
        return bookRepository.save(book);
    }

}
