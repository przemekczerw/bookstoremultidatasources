package com.przemek.czerw.synchro.database.config;

import com.przemek.czerw.synchro.database.entity.bookStore.Book;
import com.przemek.czerw.synchro.database.repository.bookStore.BookRepository;
import java.util.HashMap;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackageClasses = BookRepository.class, entityManagerFactoryRef = "bookFactory", transactionManagerRef = "bookTransactionManager")
public class BookStoreDBConfig {


    @Primary
    @Bean(name = "bookDS")
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setUrl("jdbc:mysql://localhost:3306/BookStore1?autoReconnect=true&useSSL=false");
        dataSource.setUsername("root");
        dataSource.setPassword("password");
        return dataSource;
    }


    @Primary
    @Bean(name = "bookFactory")
    public LocalContainerEntityManagerFactoryBean bookFactory(
        @Qualifier("bookDS") DataSource dataSource, EntityManagerFactoryBuilder builder) {
        HashMap<String, Object> properties = new HashMap<>();
        properties.put("hibernate.hbm2ddl.auto", "update");
        properties.put("hibernate.dialect", "org.hibernate.dialect.MySQL5Dialect");
        return builder.dataSource(dataSource).properties(properties).packages(Book.class).build();
    }

    @Primary
    @Bean(name = "bookTransactionManager")
    public PlatformTransactionManager bookTransactionManager(
        @Qualifier("bookFactory") EntityManagerFactory managerFactory) {
        return new JpaTransactionManager(managerFactory);
    }

}
