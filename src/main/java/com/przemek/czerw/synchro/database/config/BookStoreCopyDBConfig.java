package com.przemek.czerw.synchro.database.config;

import com.przemek.czerw.synchro.database.entity.bookStoreCopy.BookCopy;
import com.przemek.czerw.synchro.database.repository.bookStoreCopy.BookCopyRepository;
import java.util.HashMap;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;


@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackageClasses = BookCopyRepository.class, entityManagerFactoryRef = "bookCopyFactory", transactionManagerRef = "bookCopyTransactionManager")
public class BookStoreCopyDBConfig {


    @Bean(name = "bookCopyDS")
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setUrl("jdbc:mysql://localhost:3306/BookStore2?autoReconnect=true&useSSL=false");
        dataSource.setUsername("root");
        dataSource.setPassword("password");
        return dataSource;
    }

    @Bean(name = "bookCopyFactory")
    public LocalContainerEntityManagerFactoryBean bookCopyFactory(
        @Qualifier("bookCopyDS") DataSource dataSource, EntityManagerFactoryBuilder builder) {
        HashMap<String, Object> properties = new HashMap<>();
        properties.put("hibernate.hbm2ddl.auto", "update");
        properties.put("hibernate.dialect", "org.hibernate.dialect.MySQL5Dialect");
        return builder.dataSource(dataSource).properties(properties).packages(BookCopy.class)
            .persistenceUnit("Book2").build();
    }

    @Bean(name = "bookCopyTransactionManager")
    public PlatformTransactionManager bookCopyTransactionManager(
        @Qualifier("bookCopyFactory") EntityManagerFactory managerFactory) {
        return new JpaTransactionManager(managerFactory);
    }

}
