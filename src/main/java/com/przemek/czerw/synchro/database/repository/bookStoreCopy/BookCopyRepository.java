package com.przemek.czerw.synchro.database.repository.bookStoreCopy;

import com.przemek.czerw.synchro.database.entity.bookStoreCopy.BookCopy;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BookCopyRepository extends CrudRepository<BookCopy, Long> {

}
