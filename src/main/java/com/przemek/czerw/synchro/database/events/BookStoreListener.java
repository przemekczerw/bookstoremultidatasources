package com.przemek.czerw.synchro.database.events;

import com.przemek.czerw.synchro.database.entity.bookStore.Book;
import com.przemek.czerw.synchro.database.entity.bookStoreCopy.BookCopy;
import com.przemek.czerw.synchro.database.repository.bookStoreCopy.BookCopyRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

@Slf4j
@Service
public class BookStoreListener {

    @Autowired
    BookCopyRepository bookCopyRepository;

    @Async
    @EventListener
    public void bookStoreListener(Book event){
        log.info("---Event received from BookStore1 with element: {}", event);
        addBook2(new BookCopy(event.getId(), event.getName()));

    }

    public BookCopy addBook2(@RequestBody BookCopy bookCopy) {
        log.info("Book has been added to BookStore2: {}", bookCopy);
        return bookCopyRepository.save(bookCopy);
    }

}
